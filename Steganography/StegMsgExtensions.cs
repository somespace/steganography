﻿using System;

namespace Steganography
{
    public static class StegMsgExtensions
    {
        private const string zero = "0";
        private static string zeroPlaceholder = "`";

        public static string ResubstituteZero(this String filteredMessage)
        {
            return filteredMessage.Replace(zeroPlaceholder, zero);
        }

        public static string FilterMessage(this String message)
        {
            string filteredMessage = message.Replace(zero, zeroPlaceholder);
            return filteredMessage + zero;
        }
    }
}
