﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;

namespace Steganography
{
    class Lurk
    {
        public static Bitmap Image { get; private set; } = null;
        public static string Message { get; private set; }
        private static string ExtractedMessage { get; set; }
        public static Encoding CentralEUencoding { get; } = Encoding.GetEncoding(852);

        public static void SetMessage(string message) => Message = message;
        public static string GetExtractedMessage() => ExtractedMessage;


        public static BitArray ConvertToBitArray(string filteredMessage)
        {
            byte[] messageInBytes = CentralEUencoding.GetBytes(filteredMessage);
            return new BitArray(messageInBytes);
        }

        public static Bitmap SetImage(Bitmap bmpFile)
        {
            return Image = bmpFile;
        }

        public static void OperateOnLockedImage(Action<byte[]> operation, ImageLockMode mode)
        {
            Rectangle imgRectSelection = new Rectangle(0, 0, Image.Width, Image.Height);
            BitmapData bmpData = Image.LockBits(imgRectSelection, mode,
                Image.PixelFormat);

            IntPtr firstDataLinePtr = bmpData.Scan0;

            int bmpByteSize = Math.Abs(bmpData.Stride) * Image.Height;
            byte[] rgbByteArray = new byte[bmpByteSize];

            Marshal.Copy(firstDataLinePtr, rgbByteArray, 0, bmpByteSize);

            operation(rgbByteArray);

            Marshal.Copy(rgbByteArray, 0, firstDataLinePtr, bmpByteSize);
            Image.UnlockBits(bmpData);
        }

        public static void InjectMessage(byte[] rgbByteArray)
        {
            string filteredMessage = Message.FilterMessage();
            BitArray messageBitArray = ConvertToBitArray(filteredMessage);

            for (int i = 0, bytePos = 0; i < messageBitArray.Length; i++, bytePos += 3)
            {
                byte messageBitAsByte = Convert.ToByte(messageBitArray[i]);
                rgbByteArray[bytePos] |= messageBitAsByte;
            }
        }

        private static byte BitArrayToByte(BitArray charBits)
        {
            byte[] charAsByte = new byte[1];
            charBits.CopyTo(charAsByte, 0);

            return charAsByte[0];
        }

        public static void ExtractMessage(byte[] rgbByteArray)
        {
            string extracted = "";
            int imgPixelCount = Image.Width * Image.Height;

            int bitPos = 0;
            BitArray messageCharBits = new BitArray(8);
            for (int i = 0, bytePos = 0; i < imgPixelCount; i++, bytePos += 3)
            {
                messageCharBits[bitPos] =
                    Convert.ToBoolean((Convert.ToInt32(rgbByteArray[bytePos]) & 1));
                bitPos++;

                if ((i + 1) % 8 == 0)
                {
                    byte messageCharAsByte = BitArrayToByte(messageCharBits);
                    string messageChar =
                        CentralEUencoding.GetString(new byte[] { messageCharAsByte });

                    if (messageChar == "0")
                    {
                        break;
                    }
                    extracted += messageChar;
                    bitPos = 0;
                }
            }

            ExtractedMessage = extracted.ResubstituteZero();
        }
    }
}
