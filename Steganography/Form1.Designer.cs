﻿namespace Steganography
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.PreviewTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.InsertImageTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.AddImageButton = new System.Windows.Forms.Button();
            this.ImagePreviewGBox = new System.Windows.Forms.GroupBox();
            this.MainPictureBox = new System.Windows.Forms.PictureBox();
            this.ManipulateTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ButtonsFLPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.ExtractButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.WriteButton = new System.Windows.Forms.Button();
            this.ImageInfoGBox = new System.Windows.Forms.GroupBox();
            this.ImageInfoWrapperTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SpecialInfoTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.CharsValueLabel = new System.Windows.Forms.Label();
            this.CharsLabel = new System.Windows.Forms.Label();
            this.DimensionsValueLabel = new System.Windows.Forms.Label();
            this.DimensionsLabel = new System.Windows.Forms.Label();
            this.SizeValueLabel = new System.Windows.Forms.Label();
            this.SizeLabel = new System.Windows.Forms.Label();
            this.ExtensionValueLabel = new System.Windows.Forms.Label();
            this.ExtensionLabel = new System.Windows.Forms.Label();
            this.PathTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.PathTextBoxt = new System.Windows.Forms.TextBox();
            this.PathLabel = new System.Windows.Forms.Label();
            this.MessageGBox = new System.Windows.Forms.GroupBox();
            this.MessagePanel = new System.Windows.Forms.Panel();
            this.MessageRTBox = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.MainSplitContainer)).BeginInit();
            this.MainSplitContainer.Panel1.SuspendLayout();
            this.MainSplitContainer.Panel2.SuspendLayout();
            this.MainSplitContainer.SuspendLayout();
            this.PreviewTLPanel.SuspendLayout();
            this.InsertImageTLPanel.SuspendLayout();
            this.ImagePreviewGBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainPictureBox)).BeginInit();
            this.ManipulateTLPanel.SuspendLayout();
            this.ButtonsFLPanel.SuspendLayout();
            this.ImageInfoGBox.SuspendLayout();
            this.ImageInfoWrapperTLPanel.SuspendLayout();
            this.SpecialInfoTLPanel.SuspendLayout();
            this.PathTLPanel.SuspendLayout();
            this.MessageGBox.SuspendLayout();
            this.MessagePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainSplitContainer
            // 
            this.MainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainSplitContainer.Location = new System.Drawing.Point(10, 10);
            this.MainSplitContainer.Name = "MainSplitContainer";
            // 
            // MainSplitContainer.Panel1
            // 
            this.MainSplitContainer.Panel1.Controls.Add(this.PreviewTLPanel);
            // 
            // MainSplitContainer.Panel2
            // 
            this.MainSplitContainer.Panel2.Controls.Add(this.ManipulateTLPanel);
            this.MainSplitContainer.Size = new System.Drawing.Size(847, 511);
            this.MainSplitContainer.SplitterDistance = 389;
            this.MainSplitContainer.TabIndex = 0;
            // 
            // PreviewTLPanel
            // 
            this.PreviewTLPanel.ColumnCount = 1;
            this.PreviewTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PreviewTLPanel.Controls.Add(this.InsertImageTLPanel, 0, 1);
            this.PreviewTLPanel.Controls.Add(this.ImagePreviewGBox, 0, 0);
            this.PreviewTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PreviewTLPanel.Location = new System.Drawing.Point(0, 0);
            this.PreviewTLPanel.Name = "PreviewTLPanel";
            this.PreviewTLPanel.RowCount = 2;
            this.PreviewTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PreviewTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.PreviewTLPanel.Size = new System.Drawing.Size(389, 511);
            this.PreviewTLPanel.TabIndex = 0;
            // 
            // InsertImageTLPanel
            // 
            this.InsertImageTLPanel.ColumnCount = 1;
            this.InsertImageTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.InsertImageTLPanel.Controls.Add(this.AddImageButton, 0, 0);
            this.InsertImageTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InsertImageTLPanel.Location = new System.Drawing.Point(3, 439);
            this.InsertImageTLPanel.Name = "InsertImageTLPanel";
            this.InsertImageTLPanel.Padding = new System.Windows.Forms.Padding(5);
            this.InsertImageTLPanel.RowCount = 1;
            this.InsertImageTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.InsertImageTLPanel.Size = new System.Drawing.Size(383, 69);
            this.InsertImageTLPanel.TabIndex = 3;
            // 
            // AddImageButton
            // 
            this.AddImageButton.Location = new System.Drawing.Point(8, 8);
            this.AddImageButton.Name = "AddImageButton";
            this.AddImageButton.Size = new System.Drawing.Size(163, 40);
            this.AddImageButton.TabIndex = 0;
            this.AddImageButton.Text = "Pridať obrázok";
            this.AddImageButton.UseVisualStyleBackColor = true;
            this.AddImageButton.Click += new System.EventHandler(this.AddImageButton_Click);
            // 
            // ImagePreviewGBox
            // 
            this.ImagePreviewGBox.Controls.Add(this.MainPictureBox);
            this.ImagePreviewGBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImagePreviewGBox.Location = new System.Drawing.Point(10, 9);
            this.ImagePreviewGBox.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.ImagePreviewGBox.Name = "ImagePreviewGBox";
            this.ImagePreviewGBox.Size = new System.Drawing.Size(369, 418);
            this.ImagePreviewGBox.TabIndex = 4;
            this.ImagePreviewGBox.TabStop = false;
            this.ImagePreviewGBox.Text = "Náhľad";
            // 
            // MainPictureBox
            // 
            this.MainPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPictureBox.Location = new System.Drawing.Point(3, 22);
            this.MainPictureBox.Name = "MainPictureBox";
            this.MainPictureBox.Size = new System.Drawing.Size(363, 393);
            this.MainPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MainPictureBox.TabIndex = 0;
            this.MainPictureBox.TabStop = false;
            // 
            // ManipulateTLPanel
            // 
            this.ManipulateTLPanel.ColumnCount = 1;
            this.ManipulateTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ManipulateTLPanel.Controls.Add(this.ButtonsFLPanel, 0, 2);
            this.ManipulateTLPanel.Controls.Add(this.ImageInfoGBox, 0, 0);
            this.ManipulateTLPanel.Controls.Add(this.MessageGBox, 0, 1);
            this.ManipulateTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ManipulateTLPanel.Location = new System.Drawing.Point(0, 0);
            this.ManipulateTLPanel.Name = "ManipulateTLPanel";
            this.ManipulateTLPanel.RowCount = 3;
            this.ManipulateTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ManipulateTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ManipulateTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.ManipulateTLPanel.Size = new System.Drawing.Size(454, 511);
            this.ManipulateTLPanel.TabIndex = 0;
            // 
            // ButtonsFLPanel
            // 
            this.ButtonsFLPanel.Controls.Add(this.ExtractButton);
            this.ButtonsFLPanel.Controls.Add(this.SaveButton);
            this.ButtonsFLPanel.Controls.Add(this.WriteButton);
            this.ButtonsFLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ButtonsFLPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.ButtonsFLPanel.Location = new System.Drawing.Point(3, 439);
            this.ButtonsFLPanel.Name = "ButtonsFLPanel";
            this.ButtonsFLPanel.Padding = new System.Windows.Forms.Padding(4, 5, 0, 5);
            this.ButtonsFLPanel.Size = new System.Drawing.Size(448, 69);
            this.ButtonsFLPanel.TabIndex = 0;
            // 
            // ExtractButton
            // 
            this.ExtractButton.Location = new System.Drawing.Point(330, 8);
            this.ExtractButton.Name = "ExtractButton";
            this.ExtractButton.Size = new System.Drawing.Size(111, 40);
            this.ExtractButton.TabIndex = 2;
            this.ExtractButton.Text = "Extrahovať";
            this.ExtractButton.UseVisualStyleBackColor = true;
            this.ExtractButton.Click += new System.EventHandler(this.ExtractButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(213, 8);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(111, 40);
            this.SaveButton.TabIndex = 1;
            this.SaveButton.Text = "Uložiť";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // WriteButton
            // 
            this.WriteButton.Location = new System.Drawing.Point(96, 8);
            this.WriteButton.Name = "WriteButton";
            this.WriteButton.Size = new System.Drawing.Size(111, 40);
            this.WriteButton.TabIndex = 0;
            this.WriteButton.Text = "Zapísať";
            this.WriteButton.UseVisualStyleBackColor = true;
            this.WriteButton.Click += new System.EventHandler(this.WriteButton_Click);
            // 
            // ImageInfoGBox
            // 
            this.ImageInfoGBox.Controls.Add(this.ImageInfoWrapperTLPanel);
            this.ImageInfoGBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImageInfoGBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ImageInfoGBox.Location = new System.Drawing.Point(10, 9);
            this.ImageInfoGBox.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.ImageInfoGBox.Name = "ImageInfoGBox";
            this.ImageInfoGBox.Size = new System.Drawing.Size(434, 200);
            this.ImageInfoGBox.TabIndex = 1;
            this.ImageInfoGBox.TabStop = false;
            this.ImageInfoGBox.Text = "Podrobnosti obrázka";
            // 
            // ImageInfoWrapperTLPanel
            // 
            this.ImageInfoWrapperTLPanel.ColumnCount = 1;
            this.ImageInfoWrapperTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ImageInfoWrapperTLPanel.Controls.Add(this.SpecialInfoTLPanel, 0, 1);
            this.ImageInfoWrapperTLPanel.Controls.Add(this.PathTLPanel, 0, 0);
            this.ImageInfoWrapperTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImageInfoWrapperTLPanel.Location = new System.Drawing.Point(3, 22);
            this.ImageInfoWrapperTLPanel.Name = "ImageInfoWrapperTLPanel";
            this.ImageInfoWrapperTLPanel.RowCount = 2;
            this.ImageInfoWrapperTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.ImageInfoWrapperTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.ImageInfoWrapperTLPanel.Size = new System.Drawing.Size(428, 175);
            this.ImageInfoWrapperTLPanel.TabIndex = 0;
            // 
            // SpecialInfoTLPanel
            // 
            this.SpecialInfoTLPanel.ColumnCount = 4;
            this.SpecialInfoTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.SpecialInfoTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.SpecialInfoTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.SpecialInfoTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.SpecialInfoTLPanel.Controls.Add(this.CharsValueLabel, 3, 1);
            this.SpecialInfoTLPanel.Controls.Add(this.CharsLabel, 2, 1);
            this.SpecialInfoTLPanel.Controls.Add(this.DimensionsValueLabel, 1, 1);
            this.SpecialInfoTLPanel.Controls.Add(this.DimensionsLabel, 0, 1);
            this.SpecialInfoTLPanel.Controls.Add(this.SizeValueLabel, 3, 0);
            this.SpecialInfoTLPanel.Controls.Add(this.SizeLabel, 2, 0);
            this.SpecialInfoTLPanel.Controls.Add(this.ExtensionValueLabel, 1, 0);
            this.SpecialInfoTLPanel.Controls.Add(this.ExtensionLabel, 0, 0);
            this.SpecialInfoTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SpecialInfoTLPanel.Location = new System.Drawing.Point(3, 116);
            this.SpecialInfoTLPanel.Name = "SpecialInfoTLPanel";
            this.SpecialInfoTLPanel.RowCount = 2;
            this.SpecialInfoTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.SpecialInfoTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.SpecialInfoTLPanel.Size = new System.Drawing.Size(422, 56);
            this.SpecialInfoTLPanel.TabIndex = 0;
            // 
            // CharsValueLabel
            // 
            this.CharsValueLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CharsValueLabel.AutoSize = true;
            this.CharsValueLabel.Location = new System.Drawing.Point(318, 32);
            this.CharsValueLabel.Name = "CharsValueLabel";
            this.CharsValueLabel.Size = new System.Drawing.Size(0, 20);
            this.CharsValueLabel.TabIndex = 7;
            this.CharsValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CharsLabel
            // 
            this.CharsLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CharsLabel.AutoSize = true;
            this.CharsLabel.Location = new System.Drawing.Point(213, 32);
            this.CharsLabel.Name = "CharsLabel";
            this.CharsLabel.Size = new System.Drawing.Size(56, 20);
            this.CharsLabel.TabIndex = 6;
            this.CharsLabel.Text = "Znaky:";
            this.CharsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DimensionsValueLabel
            // 
            this.DimensionsValueLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.DimensionsValueLabel.AutoSize = true;
            this.DimensionsValueLabel.Location = new System.Drawing.Point(108, 32);
            this.DimensionsValueLabel.Name = "DimensionsValueLabel";
            this.DimensionsValueLabel.Size = new System.Drawing.Size(0, 20);
            this.DimensionsValueLabel.TabIndex = 5;
            this.DimensionsValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DimensionsLabel
            // 
            this.DimensionsLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.DimensionsLabel.AutoSize = true;
            this.DimensionsLabel.Location = new System.Drawing.Point(3, 32);
            this.DimensionsLabel.Name = "DimensionsLabel";
            this.DimensionsLabel.Size = new System.Drawing.Size(76, 20);
            this.DimensionsLabel.TabIndex = 4;
            this.DimensionsLabel.Text = "Rozmery:";
            this.DimensionsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SizeValueLabel
            // 
            this.SizeValueLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.SizeValueLabel.AutoSize = true;
            this.SizeValueLabel.Location = new System.Drawing.Point(318, 4);
            this.SizeValueLabel.Name = "SizeValueLabel";
            this.SizeValueLabel.Size = new System.Drawing.Size(0, 20);
            this.SizeValueLabel.TabIndex = 3;
            this.SizeValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SizeLabel
            // 
            this.SizeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.SizeLabel.AutoSize = true;
            this.SizeLabel.Location = new System.Drawing.Point(213, 4);
            this.SizeLabel.Name = "SizeLabel";
            this.SizeLabel.Size = new System.Drawing.Size(69, 20);
            this.SizeLabel.TabIndex = 2;
            this.SizeLabel.Text = "Veľkosť:";
            this.SizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ExtensionValueLabel
            // 
            this.ExtensionValueLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ExtensionValueLabel.AutoSize = true;
            this.ExtensionValueLabel.Location = new System.Drawing.Point(108, 4);
            this.ExtensionValueLabel.Name = "ExtensionValueLabel";
            this.ExtensionValueLabel.Size = new System.Drawing.Size(0, 20);
            this.ExtensionValueLabel.TabIndex = 1;
            this.ExtensionValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ExtensionLabel
            // 
            this.ExtensionLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ExtensionLabel.AutoSize = true;
            this.ExtensionLabel.Location = new System.Drawing.Point(3, 4);
            this.ExtensionLabel.Name = "ExtensionLabel";
            this.ExtensionLabel.Size = new System.Drawing.Size(67, 20);
            this.ExtensionLabel.TabIndex = 0;
            this.ExtensionLabel.Text = "Prípona:";
            this.ExtensionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PathTLPanel
            // 
            this.PathTLPanel.ColumnCount = 1;
            this.PathTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PathTLPanel.Controls.Add(this.PathTextBoxt, 0, 1);
            this.PathTLPanel.Controls.Add(this.PathLabel, 0, 0);
            this.PathTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PathTLPanel.Location = new System.Drawing.Point(3, 3);
            this.PathTLPanel.Name = "PathTLPanel";
            this.PathTLPanel.RowCount = 2;
            this.PathTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.PathTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PathTLPanel.Size = new System.Drawing.Size(422, 107);
            this.PathTLPanel.TabIndex = 1;
            // 
            // PathTextBoxt
            // 
            this.PathTextBoxt.BackColor = System.Drawing.SystemColors.Control;
            this.PathTextBoxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PathTextBoxt.Location = new System.Drawing.Point(8, 36);
            this.PathTextBoxt.Margin = new System.Windows.Forms.Padding(8, 6, 8, 3);
            this.PathTextBoxt.Multiline = true;
            this.PathTextBoxt.Name = "PathTextBoxt";
            this.PathTextBoxt.ReadOnly = true;
            this.PathTextBoxt.Size = new System.Drawing.Size(406, 68);
            this.PathTextBoxt.TabIndex = 2;
            // 
            // PathLabel
            // 
            this.PathLabel.AutoSize = true;
            this.PathLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PathLabel.Location = new System.Drawing.Point(5, 9);
            this.PathLabel.Margin = new System.Windows.Forms.Padding(5, 9, 5, 0);
            this.PathLabel.Name = "PathLabel";
            this.PathLabel.Size = new System.Drawing.Size(55, 20);
            this.PathLabel.TabIndex = 3;
            this.PathLabel.Text = "Cesta:";
            this.PathLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // MessageGBox
            // 
            this.MessageGBox.Controls.Add(this.MessagePanel);
            this.MessageGBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessageGBox.Location = new System.Drawing.Point(10, 227);
            this.MessageGBox.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.MessageGBox.Name = "MessageGBox";
            this.MessageGBox.Size = new System.Drawing.Size(434, 200);
            this.MessageGBox.TabIndex = 2;
            this.MessageGBox.TabStop = false;
            this.MessageGBox.Text = "Správa";
            // 
            // MessagePanel
            // 
            this.MessagePanel.Controls.Add(this.MessageRTBox);
            this.MessagePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessagePanel.Location = new System.Drawing.Point(3, 22);
            this.MessagePanel.Name = "MessagePanel";
            this.MessagePanel.Padding = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.MessagePanel.Size = new System.Drawing.Size(428, 175);
            this.MessagePanel.TabIndex = 0;
            // 
            // MessageRTBox
            // 
            this.MessageRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessageRTBox.Location = new System.Drawing.Point(10, 9);
            this.MessageRTBox.Name = "MessageRTBox";
            this.MessageRTBox.Size = new System.Drawing.Size(408, 157);
            this.MessageRTBox.TabIndex = 0;
            this.MessageRTBox.Text = "";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 531);
            this.Controls.Add(this.MainSplitContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Steganografia";
            this.MainSplitContainer.Panel1.ResumeLayout(false);
            this.MainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainSplitContainer)).EndInit();
            this.MainSplitContainer.ResumeLayout(false);
            this.PreviewTLPanel.ResumeLayout(false);
            this.InsertImageTLPanel.ResumeLayout(false);
            this.ImagePreviewGBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainPictureBox)).EndInit();
            this.ManipulateTLPanel.ResumeLayout(false);
            this.ButtonsFLPanel.ResumeLayout(false);
            this.ImageInfoGBox.ResumeLayout(false);
            this.ImageInfoWrapperTLPanel.ResumeLayout(false);
            this.SpecialInfoTLPanel.ResumeLayout(false);
            this.SpecialInfoTLPanel.PerformLayout();
            this.PathTLPanel.ResumeLayout(false);
            this.PathTLPanel.PerformLayout();
            this.MessageGBox.ResumeLayout(false);
            this.MessagePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer MainSplitContainer;
        private System.Windows.Forms.TableLayoutPanel ManipulateTLPanel;
        private System.Windows.Forms.FlowLayoutPanel ButtonsFLPanel;
        private System.Windows.Forms.Button WriteButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button ExtractButton;
        private System.Windows.Forms.TableLayoutPanel PreviewTLPanel;
        private System.Windows.Forms.GroupBox ImageInfoGBox;
        private System.Windows.Forms.GroupBox MessageGBox;
        private System.Windows.Forms.Panel MessagePanel;
        private System.Windows.Forms.RichTextBox MessageRTBox;
        private System.Windows.Forms.TableLayoutPanel ImageInfoWrapperTLPanel;
        private System.Windows.Forms.TableLayoutPanel SpecialInfoTLPanel;
        private System.Windows.Forms.Label CharsValueLabel;
        private System.Windows.Forms.Label CharsLabel;
        private System.Windows.Forms.Label DimensionsValueLabel;
        private System.Windows.Forms.Label DimensionsLabel;
        private System.Windows.Forms.Label SizeValueLabel;
        private System.Windows.Forms.Label SizeLabel;
        private System.Windows.Forms.Label ExtensionValueLabel;
        private System.Windows.Forms.Label ExtensionLabel;
        private System.Windows.Forms.TableLayoutPanel PathTLPanel;
        private System.Windows.Forms.TextBox PathTextBoxt;
        private System.Windows.Forms.TableLayoutPanel InsertImageTLPanel;
        private System.Windows.Forms.Button AddImageButton;
        private System.Windows.Forms.GroupBox ImagePreviewGBox;
        private System.Windows.Forms.PictureBox MainPictureBox;
        private System.Windows.Forms.Label PathLabel;
    }
}

