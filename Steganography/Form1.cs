﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace Steganography
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void AddImageButton_Click(object sender, System.EventArgs e)
        {
            using (OpenFileDialog openImgDialog = new OpenFileDialog())
            {
                openImgDialog.Title = "Vyberte BMP obrázok";
                openImgDialog.InitialDirectory = "c:\\";
                openImgDialog.Filter = "bmp files (*.bmp)|*.bmp";
                openImgDialog.Multiselect = false;

                if (openImgDialog.ShowDialog() == DialogResult.OK)
                {
                    var fileInfo = new FileInfo(openImgDialog.FileName);
                    string imgPath = fileInfo.FullName;
                    long imgByteSize = fileInfo.Length;
                    string imgExtension = fileInfo.Extension;

                    Bitmap bmpImg = new Bitmap(imgPath);
                    MainPictureBox.Image = Lurk.SetImage(bmpImg);

                    PathTextBoxt.Text = imgPath;
                    ExtensionValueLabel.Text = imgExtension;
                    SizeValueLabel.Text = imgByteSize.ToString() + " b";

                    int imgPixelCount = bmpImg.Width * bmpImg.Height;
                    var availableCharCount = imgPixelCount == 1 ? 0 : (imgPixelCount / 8) - 1;

                    DimensionsValueLabel.Text =
                        bmpImg.Width.ToString() + " x " + bmpImg.Height.ToString();

                    CharsValueLabel.Text = availableCharCount.ToString();
                }
            }
        }

        private void WriteButton_Click(object sender, System.EventArgs e)
        {
            int messageLength = MessageRTBox.Text.Length;
            if (MainPictureBox.Image == null)
            {
                MessageBox.Show("Pridajte prosím obrázok!",
                    "Chyba obrázka", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (messageLength == 0)
            {
                MessageBox.Show("Správa je prázdna!",
                    "Chyba správy", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (messageLength > Convert.ToInt32(CharsValueLabel.Text))
            {
                MessageBox.Show($"Dĺžka správy presahuje počet zakódovateľných znakov.",
                    "Chyba správy", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Action<byte[]> injectMessage = Lurk.InjectMessage;

            Lurk.SetMessage(MessageRTBox.Text);
            Lurk.OperateOnLockedImage(injectMessage, ImageLockMode.WriteOnly);

            MainPictureBox.Image = Lurk.Image;
            MessageBox.Show($"Správa bola úspešne vložená do obrázku.",
                    "Správa vložená", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (MainPictureBox.Image == null)
            {
                MessageBox.Show("Chýba obrázok k uloženiu",
                    "Chyba ukladania", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                using (SaveFileDialog saveImgDialog = new SaveFileDialog())
                {
                    saveImgDialog.Title = "Uložte BMP obrázok";
                    saveImgDialog.InitialDirectory = "c:\\";
                    saveImgDialog.Filter = "bmp files (*.bmp)|*.bmp";
                    saveImgDialog.FileName = "obrazok" + DimensionsValueLabel.Text.Replace(" ", "");

                    if (saveImgDialog.ShowDialog() == DialogResult.OK)
                    {
                        using (Stream imgStream = saveImgDialog.OpenFile())
                        {
                            Lurk.Image.Save(imgStream, ImageFormat.Bmp);
                            imgStream.Close();
                        }
                    }
                }
            }
        }

        private void ExtractButton_Click(object sender, EventArgs e)
        {
            Action<byte[]> extractMessage = Lurk.ExtractMessage;
            Lurk.OperateOnLockedImage(extractMessage, ImageLockMode.ReadOnly);

            string extractedMessage = Lurk.GetExtractedMessage();

            MessageBox.Show(extractedMessage, "Obsah správy",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            MessageRTBox.Text = extractedMessage;
        }
    }
}
